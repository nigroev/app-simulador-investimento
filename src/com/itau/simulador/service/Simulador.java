package com.itau.simulador.service;

import com.itau.simulador.model.Cliente;
import com.itau.simulador.model.Produto;

public class Simulador {

	public static Cliente simular(double capitalInvestido, int quantidadeMeses){
		
		Cliente retorno;
		double capitalSimulado = 0;
		Produto produto = new Produto();
		for (int i = 0; i <= quantidadeMeses; i++) {
			
		capitalSimulado += capitalInvestido * produto.getTaxaRedendimento();
		
		}
		
		retorno = new Cliente(capitalInvestido, capitalSimulado);
		
		return retorno;
	}
}
