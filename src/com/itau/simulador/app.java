package com.itau.simulador;

import com.itau.simulador.model.Cliente;
import com.itau.simulador.service.Simulador;


public class App {

	public static void main(String[] args) {
		
		double capitalInvestido = Double.isNaN(Double.parseDouble(args[0])) ? Double.parseDouble(args[0]) : Double.MAX_VALUE;
		int quantidadeMeses =  args[1].isEmpty() ? 0 : Integer.parseInt(args[1]);
		
		Cliente cliente = Simulador.simular(capitalInvestido, quantidadeMeses);
		
		System.out.println("Valor Capital Investido = " + cliente.getCapitalInvestido());
		System.out.println("Valor Capital Futuro = " + cliente.getCapitalFuturo());
		
	}
}
