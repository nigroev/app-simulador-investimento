package com.itau.simulador.model;

public class Cliente {

	private double capitalInvestido;
	
	private double capitalFuturo;
	
	public Cliente(double capitalInvestido) {
		
		this.capitalInvestido = capitalInvestido;
	}
	
	public Cliente(double capitalInvestido, double capitalFuturo) {
		
		this.capitalInvestido = capitalInvestido;
		this.capitalFuturo = capitalFuturo + capitalInvestido;
	}
	
	public double getCapitalInvestido() {
		return capitalInvestido;
	}

	public double getCapitalFuturo() {
		return capitalFuturo;
	}
	
	
}
